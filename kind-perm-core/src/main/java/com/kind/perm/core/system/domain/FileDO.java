/**  
 * @Project: jxoa
 * @Title: FileModel.java
 * @Package com.oa.commons.model
 * @date 2013-4-22 上午8:55:17
 * @Copyright: 2013 
 */
package com.kind.perm.core.system.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 类名：FileDO
 * 功能：文件保存模型
 * 详细：
 * 作者：李明
 * 版本：1.0
 * 日期：2017-01-19 上午8:55:17
 *
 */
public class FileDO implements Serializable {
	
	/**
	 * 文件    id
	 */
	private Integer id;
	/**
	 * 文件名称	包括后缀
	 */	
	private String name;
	/**
	 * 文件保存到服务器的名称，32位UUID 不包括后缀
	 */
	private String uuid;
	/**
	 * 后缀
	 */
	private String ext;
	/**
	 * 文件大小 
	 */
	private Long size;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 文件路径
	 */
	private String path;
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
