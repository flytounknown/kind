package com.kind.perm.core.system.service.impl;

import java.util.List;

import com.kind.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.dao.DictDao;
import com.kind.perm.core.system.domain.DictDO;
import com.kind.perm.core.system.service.DictService;

/**
 * 
 * 字典信息业务处理实现类. <br/>
 *
 * @date:2017-02-27 20:28:11 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class DictServiceImpl implements DictService {

	@Autowired
	private DictDao dictDao;

	@Override
	public PageView<DictDO> selectPageList(PageQuery pageQuery) {
        pageQuery.setPageSize(pageQuery.getPageSize());
		List<DictDO> list = dictDao.page(pageQuery);
		int count = dictDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(DictDO entity) throws ServiceException {
		try {
           return dictDao.saveOrUpdate(entity);
        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}

	@Override
	public DictDO getById(Long id) {
		return dictDao.getById(id);
	}

    @Override
    public void remove(Long id) throws ServiceException{
        try {
            dictDao.remove(id);

        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
    }

	@Override
	public List<DictDO> queryList(DictDO entity) {
		return dictDao.queryList(entity);
	}
}