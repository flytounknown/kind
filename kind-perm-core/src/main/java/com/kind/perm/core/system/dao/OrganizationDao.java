package com.kind.perm.core.system.dao;


import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.perm.core.system.domain.OrganizationDO;

/**
 * Function:机构信息数据访问接口. <br/>
 * @date:2017-03-02 09:24:35 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface OrganizationDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.organization.OrganizationDOMapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<OrganizationDO> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(OrganizationDO entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	OrganizationDO getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<OrganizationDO> queryList(OrganizationDO entity);
}
