package com.kind.perm.core.account.dao;

import com.kind.common.persistence.PageQuery;
import com.kind.perm.core.account.domain.AccountDO;

import java.util.List;

/**
 * Function:用户数据访问接口. <br/>
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface AccountDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.account.AccountDOMapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<AccountDO> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);
	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(AccountDO entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	AccountDO getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	/**
	 * 根据条件查询列表<br/>
	 *
	 * @param entity
	 * @return
	 */
	List<AccountDO> queryList(AccountDO entity);
    
}
