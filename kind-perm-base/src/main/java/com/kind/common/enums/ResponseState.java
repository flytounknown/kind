package com.kind.common.enums;

/**
 * @description 请求响应状态码枚举类型
 *
 * @date 2016-12-6
 *
 * @author 李明
 *
 */
public enum ResponseState {
	
	//公用
	OK(0, "OK"),
	ERROR(-1, "错误"),

	//未知错误
    ERROR_UNKNOWN(10000, "程序未知错误"),
    //数据验证失败
    ERROR_BEAN_VALIDATE_FAIL(10001, "数据验证失败"),
    //编号已存在
	ERROR_CODE_EXIST(10002, "编号已存在"),
	//Service层异常
	ERROR_CODE_SERVICE_ERROR(10003, "Service异常");

	private int code;
	private String name;

	// 构造方法
	private ResponseState(int code, String name) {
		this.code = code;
		this.name = name;
	}

	// 普通方法
	public static String getName(int code) {
		for (ResponseState o : ResponseState.values()) {
			if (o.getCode() == code) {
				return o.name;
			}
		}
		return null;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
