package com.kind.perm.wx.common.interceptor;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.kind.common.mapper.JSONMapper;
import com.kind.common.uitls.DateUtils;
import com.kind.common.uitls.IPUtils;
import com.kind.common.uitls.WxUtils;
import com.kind.perm.core.account.service.AccountService;
import com.kind.perm.core.shrio.SessionUtils;
import com.kind.perm.core.account.domain.AccountDO;
import com.kind.perm.core.system.domain.LoginLogDO;
import com.kind.perm.core.system.service.LogService;

import nl.bitwalker.useragentutils.UserAgent;
import weixin.popular.api.SnsAPI;
import weixin.popular.bean.sns.SnsToken;
import weixin.popular.bean.user.User;

/**
 * 
 * Function:日志拦截器. <br/>
 * 
 * @date:2016年5月12日 上午11:16:33 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public class SessionInterceptor implements HandlerInterceptor {

	@Autowired
	AccountService accountService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		
		AccountDO account = (AccountDO)request.getSession().getAttribute(WxUtils.WX_session_account);
		
		if(account == null){

			String query = "";
		     if(request.getQueryString()!=null && !"".equals(request.getQueryString())){
		    	 query = "?" + request.getQueryString();
		     }
		     String base = request.getRequestURL().toString();
		     if(!base.endsWith(".do")){
		    	 base=base+".do";
		     }
		     String url = (base + query).toString();
			response.sendRedirect(url);
			return false;
		}
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}
