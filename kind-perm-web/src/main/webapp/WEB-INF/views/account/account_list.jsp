<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>

<body>
    <div id="tb" style="padding:5px;height:auto">
        <div>
            <form id="searchFrom" action="">
                <input type="text" name="name" class="easyui-validatebox" data-options="width:150,prompt: '姓名'"/>
                <input type="text" name="tel" class="easyui-validatebox" data-options="width:150,prompt: '手机号'"/>
                <span class="toolbar-item dialog-tool-separator"></span>
                <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="searchFunc();">查询</a>
            </form>
            <shiro:hasPermission name="account:account:save">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addFunc();">添加</a>
            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="account:account:change">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateFunc();">修改</a>
            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="account:account:view">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="viewFunc();">查看</a>
            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="account:account:remove">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeFunc();">删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
             <shiro:hasPermission name="account:account:export">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-hamburg-home" plain="true" onclick="exportFunc();">导出</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
        </div>
    </div>

    <!--列表表格-->
    <table id="dataTable"></table>
    <!--添加、修改窗体-->
    <div id="dlg"></div>

<script type="text/javascript">
    var dg;
    var d;

    $(function(){
        dg=$('#dataTable').datagrid({
            method: "get",
            url:'${ctx}/account/account/selectPageList',
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            striped:true,
            pagination:true,
            rownumbers:true,
            pageNumber:1,
            pageSize : 20,
            pageList : [20],
            singleSelect:true,
            columns:[[
                {field:'id',title:'id',hidden:true},
                {field:'name',title:'姓名',width:20,sortable:true},
                {field:'tel',title:'手机号',width:10},
                {field:'sex',title:'性别',width:10, formatter:function(value,row,index){
                    if(row.sex=='0'){
                        return '男';
                    }else if(row.sex=='1'){
                        return '女';
                    }
                }},
                {field:'lastLoginTime',title:'上次登录时间',width:10},
                {field:'lastLoginArea',title:'上一次登陆位置',width:10}

            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar:'#tb'
        });
    });

    //弹窗增加
    function addFunc() {
        d=$("#dlg").dialog({
            title: '添加用户信息',
            width: 380,
            height: 380,
            href:'${ctx}/account/account/add',
            maximizable:true,
            modal:true,
            buttons:[{
                text:'确认',
                class: 'easyui-linkbutton c6',
                iconCls: 'icon-ok',
                handler:function(){
                    $("#mainform").submit();
                }
            },{
                text:'取消',
                iconCls: 'icon-cancel',
                handler:function(){
                    d.panel('close');
                }
            }]
        });
    }

    //弹窗修改
    function updateFunc(){
        var row = dg.datagrid('getSelected');
        if(rowIsNull(row)) return;
        d=$("#dlg").dialog({
            title: '修改用户信息',
            width: 380,
            height: 340,
            href:'${ctx}/account/account/update/'+row.id,
            maximizable:true,
            modal:true,
            buttons:[{
                text:'修改',
                iconCls: 'icon-ok',
                handler:function(){
                    $('#mainform').submit();
                }
            },{
                text:'取消',
                iconCls: 'icon-cancel',
                handler:function(){
                    d.panel('close');
                }
            }]
        });
    }

    //弹窗查看
    function viewFunc(){
        var row = dg.datagrid('getSelected');
        if(rowIsNull(row)) return;
        d=$("#dlg").dialog({
            title: '查看用户信息',
            width: 380,
            height: 340,
            href:'${ctx}/account/account/view/'+row.id,
            maximizable:true,
            modal:true,
            buttons:[]
        });
    }
    
    //删除
    function removeFunc(){
        var row = dg.datagrid('getSelected');
        if(rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复，您确定要删除吗？', function(data){
            if (data){
                $.ajax({
                    type:'get',
                    url:"${ctx}/account/account/remove/"+row.id,
                    success: function(data){
                        submitSuccess(data,dg);
                    }
                });
            }
        });
    }
    
  //导出信息
    function exportFunc(){
    	$('#searchFrom').form('submit', {
    	    url:"${ctx}/account/account/export",
    	    onSubmit: function(){
    			// do some check
    			// return false to prevent submit;
    	    },
    	    success:function(data){
    			
    	    }
    	});
    	 
    }

    //分页查询
    function searchFunc(){
        var formObj=$("#searchFrom").serializeObject();
        dg.datagrid('load',formObj);
    }

</script>
</body>
</html>