package com.kind.perm.web.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.contants.Constants;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.uitls.NumberUtils;
import com.kind.perm.core.system.domain.TableCustomDO;
import com.kind.perm.core.system.domain.TableCustomTempletDO;
import com.kind.perm.core.system.service.TableCustomService;
import com.kind.perm.core.system.service.TableCustomTempletService;
import com.kind.perm.web.common.controller.BaseController;

/**
 * 
 * Function:导出自定义管理控制器. <br/>
 * 
 * @date:2017年02月04日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("tableCustom/tableCustom")
public class TableCustomController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(TableCustomController.class);

    /**列表模板页面*/
    private final String TABLECUSTOM_TEMPLET_LIST_PAGE = "system/tableCustom/templet_tablecustom_list";
    /**表单模板页面*/
    private final String TABLECUSTOM_TEMPLET_FORM_PAGE = "system/tableCustom/templet_tablecustom_form";
    /**模板页面*/
    private final String TEMPLET_FORM_PAGE = "system/tableCustom/templet_form";
    
	@Autowired
	private TableCustomService tableCustomService;
	@Autowired
	private TableCustomTempletService tableCustomTempletService;
	
	
	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return TABLECUSTOM_TEMPLET_LIST_PAGE;
	}

	/**
	 * 全部导出工具类模板
	 * 
	 * @return
	 */
	@RequestMapping(value = "selectAllTemplet", method = RequestMethod.GET)
	@ResponseBody
	public List<TableCustomTempletDO> selectAllTemplet(@Valid TableCustomTempletDO entity) {
		List<TableCustomTempletDO> tableCustomTempletDOList = tableCustomTempletService.queryList(entity);
		return tableCustomTempletDOList;
	}
	
	
	/**
	 * 加载添加页面.
	 * 
	 * @param model
	 */
	@RequiresPermissions("tablecustomtemplet:tablecustomtemplet:save")
	@RequestMapping(value = "toAddTemplet", method = RequestMethod.GET)
	public String toAddTemplet(Model model) {
		model.addAttribute("entity", new TableCustomTempletDO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		return TEMPLET_FORM_PAGE;
	}
	
	/**
	 * 保存数据.
	 * 
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid TableCustomTempletDO entity, Model model, HttpServletRequest request) {
        try {
        	tableCustomTempletService.save(entity);
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 加载修改模板页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("tablecustomtemplet:tablecustomtemplet:change")
	@RequestMapping(value = "updateTemplet/{id}", method = RequestMethod.GET)
	public String updateTemplet(@PathVariable("id") Long id, Model model) {
		updateViewTemplet(id, model);
		
		return TEMPLET_FORM_PAGE;
	}
	
	/**
	 * 加载查看模板页面.
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "viewTemplet/{id}", method = RequestMethod.GET)
	public String viewTemplet(@PathVariable("id") Long id, Model model) {
		updateViewTemplet(id, model);
		model.addAttribute("view", "true");
		return TEMPLET_FORM_PAGE;
	}

	private void updateViewTemplet(Long id, Model model) {
		model.addAttribute("entity", tableCustomTempletService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
	}
	
	/**
	 * 修改模板数据.
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody TableCustomTempletDO entity, Model model, HttpServletRequest request) {
        try {
        	tableCustomTempletService.save(entity);
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 删除模板数据.
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("tablecustomtemplet:tablecustomtemplet:remove")
	@RequestMapping(value = "removeTemplet/{id}")
	@ResponseBody
	public JsonResponseResult removeTemplet(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
            	tableCustomTempletService.remove(id);
            }
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 获取当前模板下所有字段
	 */
	@RequestMapping("getCustomByTempletId")
	@ResponseBody
	public Map<String, Object> getCustomByTempletId(Long tbType) {
		logger.debug("tbType:" + tbType);
		Map<String, Object> map = new HashMap<String, Object>();
		if (!NumberUtils.isEmptyLong(tbType)) {
			
			TableCustomDO tableCustom = new TableCustomDO();
			tableCustom.setTbType(tbType);
			
			List<TableCustomDO> tableCustomList = tableCustomService.queryList(tableCustom);
			map.put("rows", tableCustomList);
			map.put("total", tableCustomList.size());
		}
		return map;
	}
	
	/**
	 * 跳转到为模板添加字段界面
	 */
	@RequiresPermissions("tablecustom:tablecustom:save")
	@RequestMapping(value = "addCustom/{tbType}", method = RequestMethod.GET)
	public String addCustom(@PathVariable("tbType") Long tbType, Model model) {
		TableCustomDO tableCustom = new TableCustomDO();
		tableCustom.setTbType(tbType);
		model.addAttribute("entity", tableCustom);
		model.addAttribute(Constants.CONTROLLER_ACTION, "saveCustom");
		return TABLECUSTOM_TEMPLET_FORM_PAGE;
	}
	
	
	/**
	 * 保存模板添加字段数据.
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "saveCustom", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid TableCustomDO entity, Model model, HttpServletRequest request) {
        try {
        	tableCustomService.save(entity);
            return JsonResponseResult.success();
        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 加载修改模板添加字段页面.
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("tablecustom:tablecustom:change")
	@RequestMapping(value = "updateCustom/{id}", method = RequestMethod.GET)
	public String updateCustom(@PathVariable("id") Long id, Model model) {
		updateViewCustom(id, model);
		
		return TABLECUSTOM_TEMPLET_FORM_PAGE;
	}
	
	/**
	 * 加载查看模板添加字段页面.
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("tablecustom:tablecustom:view")
	@RequestMapping(value = "viewCustom/{id}", method = RequestMethod.GET)
	public String viewCustom(@PathVariable("id") Long id, Model model) {
		updateViewCustom(id, model);
		model.addAttribute("view", "true");
		return TABLECUSTOM_TEMPLET_FORM_PAGE;
	}

	private void updateViewCustom(Long id, Model model) {
		model.addAttribute("entity", tableCustomService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, "updateCustom");
	}
	
	/**
	 * 修改模板添加字段数据.
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "updateCustom", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult updateCustom(@Valid @ModelAttribute @RequestBody TableCustomDO entity, Model model, HttpServletRequest request) {
        try {
        	tableCustomService.save(entity);
        	
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 删除模板添加字段数据.
	 * @param id
	 * @return
	 */
	@RequiresPermissions("tablecustom:tablecustom:remove")
	@RequestMapping(value = "removeCustom/{id}")
	@ResponseBody
	public JsonResponseResult removeCustom(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
            	tableCustomService.remove(id);
            }
            return JsonResponseResult.success();
        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	
}
